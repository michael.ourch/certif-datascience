import unittest
from unittest.mock import patch

import pandas as pd
from octo_fly_use_case.train import train

from octo_fly_use_case.predict import predict_and_save


class MyTestCase(unittest.TestCase):
    @patch("pandas.DataFrame.to_csv")
    @patch("pickle.dump")
    def test_predict_and_save_method_return_model_trained_linear_regression(
        self, mock_pandas_csv, mock_pickle_dump
    ):
        # Given
        vols_test_df = pd.DataFrame(
            {
                "IDENTIFIANT": {0: 1259209, 1: 4886177, 2: 183332, 3: 937517},
                "VOL": {0: 4661, 1: 5026, 2: 2021, 3: 1320},
                "CODE AVION": {0: 1, 1: 0, 2: 3, 3: 2},
                "AEROPORT DEPART": {0: 1, 1: 3, 2: 2, 3: 0},
                "AEROPORT ARRIVEE": {0: 0, 1: 3, 2: 2, 3: 1},
                "DEPART PROGRAMME": {0: 1707, 1: 600, 2: 1749, 3: 2301},
                "DECOLLAGE": {0: 1718.0, 1: 604.0, 2: 1756.0, 3: 2341.0},
                "TEMPS PROGRAMME": {0: 67.0, 1: 130.0, 2: 248.0, 3: 65.0},
                "TEMPS PASSE": {0: 71.0, 1: 119.0, 2: 228.0, 3: 89.0},
                "TEMPS DE VOL": {0: 45.0, 1: 91.0, 2: 215.0, 3: 59.0},
                "DISTANCE": {0: 232, 1: 738, 2: 1671, 3: 214},
                "ATTERRISSAGE": {0: 1803.0, 1: 835.0, 2: 1831.0, 3: 40.0},
                "NOMBRE DE PASSAGERS": {0: 379, 1: 9, 2: 2491, 3: 1241},
                "DATE": {0: 0, 1: 1, 2: 3, 3: 2},
                "NIVEAU DE SECURITE": {0: 10, 1: 10, 2: 10, 3: 10},
            }
        )
        train_df = pd.DataFrame(
            {
                "IDENTIFIANT": {3: 937517},
                "VOL": {3: 1320},
                "CODE AVION": {3: 2},
                "AEROPORT DEPART": {3: 0},
                "AEROPORT ARRIVEE": {3: 1},
                "DEPART PROGRAMME": {3: 2301},
                "HEURE DE DEPART": {3: 2322.0},
                "RETART DE DEPART": {3: 21.0},
                "TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE": {3: 19.0},
                "DECOLLAGE": {3: 2341.0},
                "TEMPS PROGRAMME": {3: 65.0},
                "TEMPS PASSE": {3: 89.0},
                "TEMPS DE VOL": {3: 59.0},
                "DISTANCE": {3: 214},
                "ATTERRISSAGE": {3: 40.0},
                "TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE": {3: 11.0},
                "ARRIVEE PROGRAMMEE": {3: 6},
                "HEURE D'ARRIVEE": {3: 51.0},
                "RETARD A L'ARRIVEE": {3: 45.0},
                "DETOURNEMENT": {3: 0},
                "ANNULATION": {3: 0},
                "RAISON D'ANNULATION": {3: -1},
                "RETARD SYSTEM": {3: 24.0},
                "RETARD SECURITE": {3: 0.0},
                "RETARD COMPAGNIE": {3: 0.0},
                "RETARD AVION": {3: 6.0},
                "RETARD METEO": {3: 15.0},
                "COMPAGNIE AERIENNE": {3: 2},
                "NOMBRE DE PASSAGERS": {3: 1241},
                "DATE": {3: 2},
                "NIVEAU DE SECURITE": {3: 10},
            }
        )
        model = train(train_df)
        # When
        predict_and_save(vols_test_df, model)
        # Then
        mock_pandas_csv.assert_called_once()
        mock_pickle_dump.mock_pickle_dump()
