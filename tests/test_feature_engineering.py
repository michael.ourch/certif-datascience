import unittest

import pandas as pd
from octo_fly_use_case.feature_engineering import (
    convert_object_columns_to_category_type,
    drop_cancelled_and_hijacked,
    feature_engineering_for_prediction,
    feature_engineering_for_training,
)


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.maxDiff = None
        self.vols_df = pd.DataFrame(
            {
                "IDENTIFIANT": {0: 1259209, 1: 4886177, 2: 183332, 3: 937517},
                "VOL": {0: 4661, 1: 5026, 2: 2021, 3: 1320},
                "CODE AVION": {
                    0: "a02782cd75",
                    1: "707f6ea54f",
                    2: "b116987956",
                    3: "a4b8db63f5",
                },
                "AEROPORT DEPART": {0: "CEB", 1: "GOI", 2: "DSS", 3: "AGP"},
                "AEROPORT ARRIVEE": {0: "AAL", 1: "LTK", 2: "JNB", 3: "GOA"},
                "DEPART PROGRAMME": {0: 1707, 1: 600, 2: 1749, 3: 2301},
                "HEURE DE DEPART": {0: 1658.0, 1: 553.0, 2: 1747.0, 3: 2322.0},
                "RETART DE DEPART": {0: -9.0, 1: -7.0, 2: -2.0, 3: 21.0},
                "TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE": {
                    0: 20.0,
                    1: 11.0,
                    2: 9.0,
                    3: 19.0,
                },
                "DECOLLAGE": {0: 1718.0, 1: 604.0, 2: 1756.0, 3: 2341.0},
                "TEMPS PROGRAMME": {0: 67.0, 1: 130.0, 2: 248.0, 3: 65.0},
                "TEMPS PASSE": {0: 71.0, 1: 119.0, 2: 228.0, 3: 89.0},
                "TEMPS DE VOL": {0: 45.0, 1: 91.0, 2: 215.0, 3: 59.0},
                "DISTANCE": {0: 232, 1: 738, 2: 1671, 3: 214},
                "ATTERRISSAGE": {0: 1803.0, 1: 835.0, 2: 1831.0, 3: 40.0},
                "TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE": {
                    0: 6.0,
                    1: 17.0,
                    2: 4.0,
                    3: 11.0,
                },
                "ARRIVEE PROGRAMMEE": {0: 1814, 1: 910, 2: 1857, 3: 6},
                "HEURE D'ARRIVEE": {0: 1809.0, 1: 852.0, 2: 1835.0, 3: 51.0},
                "RETARD A L'ARRIVEE": {0: -5.0, 1: -18.0, 2: -22.0, 3: 45.0},
                "DETOURNEMENT": {0: 0, 1: 0, 2: 1, 3: 0},
                "ANNULATION": {0: 1, 1: 1, 2: 0, 3: 0},
                "RAISON D'ANNULATION": {0: None, 1: None, 2: None, 3: None},
                "RETARD SYSTEM": {0: None, 1: None, 2: None, 3: 24.0},
                "RETARD SECURITE": {0: None, 1: None, 2: None, 3: 0.0},
                "RETARD COMPAGNIE": {0: None, 1: None, 2: None, 3: 0.0},
                "RETARD AVION": {0: None, 1: None, 2: None, 3: 6.0},
                "RETARD METEO": {0: None, 1: None, 2: None, 3: 15.0},
                "COMPAGNIE AERIENNE": {0: "MAF", 1: "I6F", 2: "NVPPA", 3: "NVPPA"},
                "NOMBRE DE PASSAGERS": {0: 379, 1: 9, 2: 2491, 3: 1241},
                "DATE": {0: "15/8/2018", 1: "2/11/2016", 2: "9/6/2017", 3: "26/5/2018"},
                "NIVEAU DE SECURITE": {0: 10, 1: 10, 2: 10, 3: 10},
            }
        )

    def test_object_columns_are_converted_to_category_type(self):
        # Given
        # When
        converted_df = convert_object_columns_to_category_type(self.vols_df)
        # Then
        for dtype in converted_df.dtypes:
            self.assertNotEqual("object", dtype.type)

    def test_cancelled_or_hijacked_vols_are_deleted(self):
        # Given
        expected_dataframe = pd.DataFrame(
            {
                "IDENTIFIANT": {3: 937517},
                "VOL": {3: 1320},
                "CODE AVION": {3: "a4b8db63f5"},
                "AEROPORT DEPART": {3: "AGP"},
                "AEROPORT ARRIVEE": {3: "GOA"},
                "DEPART PROGRAMME": {3: 2301},
                "HEURE DE DEPART": {3: 2322.0},
                "RETART DE DEPART": {3: 21.0},
                "TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE": {3: 19.0},
                "DECOLLAGE": {3: 2341.0},
                "TEMPS PROGRAMME": {3: 65.0},
                "TEMPS PASSE": {3: 89.0},
                "TEMPS DE VOL": {3: 59.0},
                "DISTANCE": {3: 214},
                "ATTERRISSAGE": {3: 40.0},
                "TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE": {3: 11.0},
                "ARRIVEE PROGRAMMEE": {3: 6},
                "HEURE D'ARRIVEE": {3: 51.0},
                "RETARD A L'ARRIVEE": {3: 45.0},
                "DETOURNEMENT": {3: 0},
                "ANNULATION": {3: 0},
                "RAISON D'ANNULATION": {3: None},
                "RETARD SYSTEM": {3: 24.0},
                "RETARD SECURITE": {3: 0.0},
                "RETARD COMPAGNIE": {3: 0.0},
                "RETARD AVION": {3: 6.0},
                "RETARD METEO": {3: 15.0},
                "COMPAGNIE AERIENNE": {3: "NVPPA"},
                "NOMBRE DE PASSAGERS": {3: 1241},
                "DATE": {3: "26/5/2018"},
                "NIVEAU DE SECURITE": {3: 10},
            }
        )

        # When
        response = drop_cancelled_and_hijacked(self.vols_df)

        # Then
        pd.testing.assert_frame_equal(expected_dataframe, response)

    def test_feature_engineering_for_prediction_returns_df_with_no_na_for_two_columns(
        self,
    ):
        # Given
        # When
        df_converted = feature_engineering_for_prediction(self.vols_df)
        # Then
        self.assertFalse(
            df_converted["TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE"].isna().any()
        )
        self.assertFalse(
            df_converted["TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE"].isna().any()
        )

    def test_feature_engineering_for_training_returns_df_without_canceled_and_hijacked(
        self,
    ):
        # Given
        # When
        df_converted = feature_engineering_for_training(self.vols_df)
        # Then
        self.assertEqual(0, df_converted["ANNULATION"].sum())
        self.assertEqual(0, df_converted["DETOURNEMENT"].sum())
