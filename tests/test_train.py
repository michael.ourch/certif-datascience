import unittest

import pandas as pd
from sklearn.linear_model import LinearRegression

from octo_fly_use_case.train import train


class MyTestCase(unittest.TestCase):
    def test_train_method_return_model_trained_linear_regression(self):
        # Given
        train_df = pd.DataFrame(
            {
                "IDENTIFIANT": {3: 937517},
                "VOL": {3: 1320},
                "CODE AVION": {3: 2},
                "AEROPORT DEPART": {3: 0},
                "AEROPORT ARRIVEE": {3: 1},
                "DEPART PROGRAMME": {3: 2301},
                "HEURE DE DEPART": {3: 2322.0},
                "RETART DE DEPART": {3: 21.0},
                "TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE": {3: 19.0},
                "DECOLLAGE": {3: 2341.0},
                "TEMPS PROGRAMME": {3: 65.0},
                "TEMPS PASSE": {3: 89.0},
                "TEMPS DE VOL": {3: 59.0},
                "DISTANCE": {3: 214},
                "ATTERRISSAGE": {3: 40.0},
                "TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE": {3: 11.0},
                "ARRIVEE PROGRAMMEE": {3: 6},
                "HEURE D'ARRIVEE": {3: 51.0},
                "RETARD A L'ARRIVEE": {3: 45.0},
                "DETOURNEMENT": {3: 0},
                "ANNULATION": {3: 0},
                "RAISON D'ANNULATION": {3: -1},
                "RETARD SYSTEM": {3: 24.0},
                "RETARD SECURITE": {3: 0.0},
                "RETARD COMPAGNIE": {3: 0.0},
                "RETARD AVION": {3: 6.0},
                "RETARD METEO": {3: 15.0},
                "COMPAGNIE AERIENNE": {3: 2},
                "NOMBRE DE PASSAGERS": {3: 1241},
                "DATE": {3: 2},
                "NIVEAU DE SECURITE": {3: 10},
            }
        )
        # When
        model_response = train(train_df)
        # Then
        self.assertEqual(LinearRegression, type(model_response))
