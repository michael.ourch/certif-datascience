import unittest
from unittest.mock import patch

import pandas as pd

from octo_fly_use_case.preprocessing import (
    load_as_a_dataframe,
    create_connection,
    split_into_X_and_Y,
    load_train_dataset,
    load_test_dataset,
)


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.maxDiff = None
        self.filepath = "tests/data/input/test_data.db"
        self.connection = create_connection(self.filepath)

    @patch("octo_fly_use_case.preprocessing.create_connection")
    def test_load_train_dataset(self, mock_data_input_path):
        # Given
        mock_data_input_path.return_value = create_connection(self.filepath)
        # When
        vols, aeroports, prix_fuel, compagnies = load_train_dataset()
        # Then
        self.assertEqual(pd.DataFrame, type(vols))
        self.assertEqual(pd.DataFrame, type(aeroports))
        self.assertEqual(pd.DataFrame, type(prix_fuel))
        self.assertEqual(pd.DataFrame, type(compagnies))

    def test_train_data_are_loaded_as_a_dataframe(self):
        # Given
        table = "vols"

        # When
        response = load_as_a_dataframe(self.connection, table)

        # Then
        self.assertEqual(pd.DataFrame, type(response))

    def test_table_vols_has_correct_columns(self):
        # Given
        table = "vols"
        columns = [
            "IDENTIFIANT",
            "VOL",
            "CODE AVION",
            "AEROPORT DEPART",
            "AEROPORT ARRIVEE",
            "DEPART PROGRAMME",
            "HEURE DE DEPART",
            "RETART DE DEPART",
            "TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE",
            "DECOLLAGE",
            "TEMPS PROGRAMME",
            "TEMPS PASSE",
            "TEMPS DE VOL",
            "DISTANCE",
            "ATTERRISSAGE",
            "TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE",
            "ARRIVEE PROGRAMMEE",
            "HEURE D'ARRIVEE",
            "RETARD A L'ARRIVEE",
            "DETOURNEMENT",
            "ANNULATION",
            "RAISON D'ANNULATION",
            "RETARD SYSTEM",
            "RETARD SECURITE",
            "RETARD COMPAGNIE",
            "RETARD AVION",
            "RETARD METEO",
            "COMPAGNIE AERIENNE",
            "NOMBRE DE PASSAGERS",
            "DATE",
            "NIVEAU DE SECURITE",
        ]

        # When
        response = load_as_a_dataframe(self.connection, table)

        # Then
        self.assertListEqual(columns, list(response.columns))

    def test_table_aeroports_has_correct_columns(self):
        # Given
        table = "aeroports"
        columns = [
            "CODE IATA",
            "NOM",
            "LIEU",
            "PAYS",
            "LONGITUDE",
            "LATITUDE",
            "HAUTEUR",
            "PRIX RETARD PREMIERE 20 MINUTES",
            "PRIS RETARD POUR CHAQUE MINUTE APRES 10 MINUTES",
        ]

        # When
        response = load_as_a_dataframe(self.connection, table)

        # Then
        self.assertListEqual(columns, list(response.columns))

    def test_table_compagnies_has_correct_columns(self):
        # Given
        table = "compagnies"
        columns = ["COMPAGNIE", "CODE", "NOMBRE D EMPLOYES", "CHIFFRE D AFFAIRE"]

        # When
        response = load_as_a_dataframe(self.connection, table)

        # Then
        self.assertListEqual(columns, list(response.columns))

    def test_table_prix_fuel_has_correct_columns(self):
        # Given
        table = "prix_fuel"
        columns = ["DATE", "PRIX DU BARIL"]

        # When
        response = load_as_a_dataframe(self.connection, table)
        # Then
        self.assertListEqual(columns, list(response.columns))

    def test_split_into_X_and_Y_return_correct_train_dataframe(self):
        # Given
        train_df = pd.DataFrame(
            {
                "IDENTIFIANT": {3: 937517},
                "VOL": {3: 1320},
                "CODE AVION": {3: "a4b8db63f5"},
                "AEROPORT DEPART": {3: "AGP"},
                "AEROPORT ARRIVEE": {3: "GOA"},
                "DEPART PROGRAMME": {3: 2301},
                "HEURE DE DEPART": {3: 2322.0},
                "RETART DE DEPART": {3: 21.0},
                "TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE": {3: 19.0},
                "DECOLLAGE": {3: 2341.0},
                "TEMPS PROGRAMME": {3: 65.0},
                "TEMPS PASSE": {3: 89.0},
                "TEMPS DE VOL": {3: 59.0},
                "DISTANCE": {3: 214},
                "ATTERRISSAGE": {3: 40.0},
                "TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE": {3: 11.0},
                "ARRIVEE PROGRAMMEE": {3: 6},
                "HEURE D'ARRIVEE": {3: 51.0},
                "RETARD A L'ARRIVEE": {3: 45.0},
                "DETOURNEMENT": {3: 0},
                "ANNULATION": {3: 0},
                "RAISON D'ANNULATION": {3: None},
                "RETARD SYSTEM": {3: 24.0},
                "RETARD SECURITE": {3: 0.0},
                "RETARD COMPAGNIE": {3: 0.0},
                "RETARD AVION": {3: 6.0},
                "RETARD METEO": {3: 15.0},
                "COMPAGNIE AERIENNE": {3: "NVPPA"},
                "NOMBRE DE PASSAGERS": {3: 1241},
                "DATE": {3: "26/5/2018"},
                "NIVEAU DE SECURITE": {3: 10},
            }
        )
        expected_x_data = pd.DataFrame(
            {
                "IDENTIFIANT": {3: 937517},
                "VOL": {3: 1320},
                "CODE AVION": {3: "a4b8db63f5"},
                "AEROPORT DEPART": {3: "AGP"},
                "AEROPORT ARRIVEE": {3: "GOA"},
                "DEPART PROGRAMME": {3: 2301},
                "TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE": {3: 19.0},
                "TEMPS PROGRAMME": {3: 65.0},
                "DISTANCE": {3: 214},
                "TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE": {3: 11.0},
                "ARRIVEE PROGRAMMEE": {3: 6},
                "COMPAGNIE AERIENNE": {3: "NVPPA"},
                "NOMBRE DE PASSAGERS": {3: 1241},
                "DATE": {3: "26/5/2018"},
                "NIVEAU DE SECURITE": {3: 10},
            }
        )
        expected_y_data = pd.Series({3: 45.0}, name="RETARD A L'ARRIVEE")
        # When
        x_data, y_data = split_into_X_and_Y(train_df)

        # Then
        pd.testing.assert_frame_equal(expected_x_data, x_data)
        pd.testing.assert_series_equal(expected_y_data, y_data)

    @patch("octo_fly_use_case.preprocessing.create_connection")
    def test_load_test_dataset(self, mock_data_input_path):
        # Given
        mock_data_input_path.return_value = create_connection(self.filepath)
        # When
        response_dataframe = load_test_dataset()
        # Then
        self.assertEqual(pd.DataFrame, type(response_dataframe))
