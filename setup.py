from setuptools import setup

setup(
    name='octo-fly-use-case',
    version='1.0.0',
    packages=['octo_fly_use_case'],
    url='https://gitlab.com/michael.ourch/certif-datascience/-/tree/main',
    author='khna-oumi',
    author_email='oumi@octo.com, khna@octo.com',
    description='A package to predict the delay of a fly.'
)
