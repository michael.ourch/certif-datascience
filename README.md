# Certif Datascience

Certification octo datascience OUMI KHNA

## Objectif

Prédire le retard des avions

##  Installation du package
```
python setup.py install
```

## Exécution de l'application

- ```git pull``` 
- ```git-lfs install``` (```brew install git-lfs``` si besoin)
- ```git-lfs pull``` pour obtenir les fichiers data *.db
- Installer les dépendences de l'environnement virtuel : ```pipenv install --dev``` (dev pour le coverage)
- Exécuter le script de prédiction with IDE (configurer l'interpreteur en choisissant pipenv)

## Coverage
- ```pipenv shell```
- ```coverage run --source=octo_fly_use_case -m unittest```
- ```coverage report```

## Installer le package
- ```python setup.py install```