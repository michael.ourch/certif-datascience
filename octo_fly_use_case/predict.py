import pickle
from datetime import datetime

import pandas as pd
from sklearn.linear_model import LinearRegression

from data.input.columns import COLUMN_TO_PREDICT

OUTPUT_FILEPATH = "../data/output/"


def predict_and_save(X_test: pd.DataFrame, model: LinearRegression):
    prediction = model.predict(X_test)
    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")
    csv_filepath = f"{OUTPUT_FILEPATH}predictions_csv/predictions_{dt_string}.csv"
    pickle_model_filepath = f"{OUTPUT_FILEPATH}models/model_{dt_string}.sav"

    # TODO: we should keep index because some rows are deleted before
    pd.DataFrame(prediction, columns=[COLUMN_TO_PREDICT]).to_csv(csv_filepath)
    pickle.dump(model, open(pickle_model_filepath, "wb"))
