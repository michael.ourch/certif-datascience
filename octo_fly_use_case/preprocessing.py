import sqlite3
from sqlite3 import Connection

import pandas as pd

from data.input.columns import COLUMNS_USED_TO_TRAIN, COLUMN_TO_PREDICT

DATA_INPUT_PATH = "../data/input/batch_2.db"


def split_into_X_and_Y(train: pd.DataFrame) -> tuple:
    return train[COLUMNS_USED_TO_TRAIN], train[COLUMN_TO_PREDICT]


def load_train_dataset() -> tuple:
    con = create_connection(DATA_INPUT_PATH)
    vols = load_as_a_dataframe(con, "vols")
    aeroports = load_as_a_dataframe(con, "aeroports")
    prix_fuel = load_as_a_dataframe(con, "prix_fuel")
    compagnies = load_as_a_dataframe(con, "compagnies")
    return vols, aeroports, prix_fuel, compagnies


def load_test_dataset() -> pd.DataFrame:
    con = create_connection("../data/input/test.db")
    return pd.read_sql_query("SELECT * FROM vols", con)


def load_as_a_dataframe(connection: Connection, table: str) -> pd.DataFrame:
    return pd.read_sql_query(f"SELECT * FROM {table}", connection)


def create_connection(filepath) -> Connection:
    return sqlite3.connect(filepath)
