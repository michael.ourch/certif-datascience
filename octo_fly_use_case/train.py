import pandas as pd
from sklearn.linear_model import LinearRegression

from octo_fly_use_case.preprocessing import split_into_X_and_Y


def train(train: pd.DataFrame) -> LinearRegression:
    linear_regression_model = LinearRegression()
    X_train, Y_train = split_into_X_and_Y(train)
    linear_regression_model.fit(X_train, Y_train)
    return linear_regression_model
