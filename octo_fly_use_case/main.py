from octo_fly_use_case.feature_engineering import feature_engineering_for_prediction, feature_engineering_for_training
from octo_fly_use_case.predict import predict_and_save
from octo_fly_use_case.preprocessing import load_train_dataset, load_test_dataset
from octo_fly_use_case.train import train

if __name__ == '__main__':
    vols_train_df, _, _, _ = load_train_dataset()
    vols_test_df = load_test_dataset()

    vols_train_df = feature_engineering_for_training(vols_train_df)
    vols_test_df = feature_engineering_for_prediction(vols_test_df)

    model = train(vols_train_df)
    predict_and_save(vols_test_df, model)
