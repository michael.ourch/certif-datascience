import pandas as pd


def feature_engineering_for_training(df: pd.DataFrame) -> pd.DataFrame:
    df_converted = convert_object_columns_to_category_type(df.copy())
    return drop_cancelled_and_hijacked(df_converted)


def feature_engineering_for_prediction(df: pd.DataFrame) -> pd.DataFrame:
    df_converted = convert_object_columns_to_category_type(df.copy())
    df_converted = df_converted[df_converted["TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE"].notna()]
    df_converted = df_converted[df_converted["TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE"].notna()]
    return df_converted


def convert_object_columns_to_category_type(df: pd.DataFrame) -> pd.DataFrame:
    for col in list(df.select_dtypes(include=['object']).columns):
        df[col] = df[col].astype('category')
        df[col] = df[col].cat.codes
    return df


def drop_cancelled_and_hijacked(df: pd.DataFrame) -> pd.DataFrame:
    res = df.copy()
    res = res[res["ANNULATION"] == 0]
    res = res[res["DETOURNEMENT"] == 0]
    return res
