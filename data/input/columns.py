COLUMN_TO_PREDICT = "RETARD A L'ARRIVEE"
COLUMNS_USED_TO_TRAIN = ["IDENTIFIANT", "VOL", "CODE AVION",
                         "AEROPORT DEPART",
                         "AEROPORT ARRIVEE",
                         "DEPART PROGRAMME",
                         "TEMPS DE DEPLACEMENT A TERRE AU DECOLLAGE",
                         "TEMPS PROGRAMME",
                         "DISTANCE",
                         "TEMPS DE DEPLACEMENT A TERRE A L'ATTERRISSAGE",
                         "ARRIVEE PROGRAMMEE",
                         "COMPAGNIE AERIENNE",
                         "NOMBRE DE PASSAGERS",
                         "DATE",
                         "NIVEAU DE SECURITE"]
